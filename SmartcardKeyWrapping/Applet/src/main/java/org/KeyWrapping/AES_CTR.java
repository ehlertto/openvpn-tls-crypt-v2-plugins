package org.KeyWrapping;

import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.CryptoException;
import javacard.security.Key;
import javacardx.crypto.Cipher;

public class AES_CTR extends Cipher {
    public static final byte AES_BLOCK_SIZE = 16;
    public static final byte IV_SIZE = 16;
    private Cipher aesEngine = null;

    private byte[] iv = null;
    private byte[] ivsequence = null;


    public AES_CTR() {
        aesEngine = Cipher.getInstance(Cipher.ALG_AES_BLOCK_128_ECB_NOPAD, false);
        iv = JCSystem.makeTransientByteArray(IV_SIZE, JCSystem.CLEAR_ON_DESELECT);
        ivsequence = JCSystem.makeTransientByteArray(WrapApplet.MAX_WKC_SIZE, JCSystem.CLEAR_ON_DESELECT);
    }

    @Override
    public void init(Key key, byte mode) throws CryptoException {
        aesEngine.init(key, Cipher.MODE_ENCRYPT);
    }

    @Override
    public void init(Key key, byte mode, byte[] nonce, short nOff, short nLen) throws CryptoException {
        if (nLen != AES_BLOCK_SIZE) {
            throw new CryptoException(CryptoException.ILLEGAL_VALUE);
        }
        Util.arrayCopy(nonce, nOff, this.iv, (short) 0, nLen);
        this.init(key, Cipher.MODE_ENCRYPT);
    }

    @Override
    public short doFinal(byte[] inBuff, short inOffset, short inLength, byte[] outBuff, short outOffset) throws CryptoException {
        // short blockCount = ceil(inLength / AES_BLOCK_SIZE);
        short blockCount = (short) ((short) (inLength + ((short) AES_BLOCK_SIZE - 1)) / AES_BLOCK_SIZE);
        for (short blockIndex = 0; blockIndex < blockCount; blockIndex += 1) {
            Util.arrayCopyNonAtomic(iv, (short) 0, ivsequence, (short) (blockIndex * AES_BLOCK_SIZE), AES_BLOCK_SIZE);
            incrementByteArray(iv);
        }

        aesEngine.doFinal(ivsequence, (short) 0, (short) (blockCount * AES_BLOCK_SIZE), ivsequence, (short) 0);

        xorByteArray(outBuff, outOffset, ivsequence, (short) 0, inLength);
        return inLength;
    }

    @Override
    public short update(byte[] inBuff, short inOffset, short inLength, byte[] outBuff, short outOffset) throws CryptoException {
        ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED);
        return 0;
    }

    private void xorByteArray(byte[] a, short aOffset, byte[] b, short bOffset, short len) {
        for (short i = 0; i < len; i++) {
            a[(short) (aOffset + i)] ^= b[(short) (bOffset + i)];
        }
    }

    private static void incrementByteArray(byte[] bArray) {
        short carry = 1;
        for (short i = (short) (bArray.length - 1); i >= 0 && carry == 1; i--) {
            bArray[i] += carry;
            carry = (short) (bArray[i] == 0 ? 1 : 0);
        }
    }


    @Override
    public byte getAlgorithm() {
        return 0;
    }

    /* Required for JCDK 3.0.5
    @Override
    public byte getPaddingAlgorithm() { return PAD_NULL; }
    @Override
    public byte getCipherAlgorithm() { return 0; }
    */
}
