package org.KeyWrapping;

import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.MessageDigest;

public class HMAC {
    public static final short SHA256_BLOCK_SIZE = 64;

    public static final short SHA1_BLOCK_SIZE = 64;

    public byte[] currentMDSpecs;

    private static final byte IPAD = 0x36;
    private static final byte OPAD = 0x5C;
    private MessageDigest mdEngine = null;
    private byte[] keyBuffer = null;

    public HMAC(byte mode) {
        if (mode == MessageDigest.ALG_SHA) {
            currentMDSpecs = new byte[]{MessageDigest.LENGTH_SHA, SHA1_BLOCK_SIZE};
        } else if (mode == MessageDigest.ALG_SHA_256) {
            currentMDSpecs = new byte[]{MessageDigest.LENGTH_SHA_256, SHA256_BLOCK_SIZE};
        } else {
            ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED);
        }

        mdEngine = MessageDigest.getInstance(mode, false);
        keyBuffer = JCSystem.makeTransientByteArray(currentMDSpecs[1], JCSystem.CLEAR_ON_DESELECT);
        Util.arrayFillNonAtomic(keyBuffer, (short) 0, (short) keyBuffer.length, (byte) 0);
    }

    public void init(HMACKey key, byte mode) {
        if (mode != mdEngine.getAlgorithm()) {
            ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED);
        }

        short keyLength = (short) (key.getSize() / 8);
        key.getKey(keyBuffer, (short) 0);

        createPaddedKey(IPAD, keyBuffer, keyBuffer, keyLength, currentMDSpecs[1]);
        // Already start the engine
        mdEngine.update(keyBuffer, (short) 0, currentMDSpecs[1]);

        key.getKey(keyBuffer, (short) 0);
        createPaddedKey(OPAD, keyBuffer, keyBuffer, keyLength, currentMDSpecs[1]);
    }

    public short doFinal(byte[] inBuff, short inOffset, short inLength, byte[] outBuff, short outOffset) {
        mdEngine.doFinal(inBuff, inOffset, inLength, outBuff, outOffset);

        mdEngine.update(keyBuffer, (short) 0, currentMDSpecs[1]);
        mdEngine.doFinal(outBuff, outOffset, currentMDSpecs[0], outBuff, outOffset);

        Util.arrayFillNonAtomic(keyBuffer, (short) 0, (short) keyBuffer.length, (byte) 0);

        return currentMDSpecs[0];
    }

    public void update(byte[] inBuff, short inOffset, short inLength) {
        mdEngine.update(inBuff, inOffset, inLength);
    }

    private void createPaddedKey(byte pattern, byte[] byteKey, byte[] hmacKey, short keyLength, short blockSize) {
        for (short i = 0; i < keyLength; i++) {
            hmacKey[i] = (byte) (pattern ^ byteKey[i]);
        }
        Util.arrayFillNonAtomic(hmacKey, keyLength, (short) (blockSize - keyLength), pattern);
    }
}
