package org.KeyWrapping;


import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.CryptoException;
import javacard.security.MessageDigest;

public class HMACKey implements javacard.security.HMACKey {
    private byte[] key = null;
    private boolean initialized_flag = false;
    private final byte type;
    public byte[] currentMDSpecs;

    public HMACKey(byte type) {
        if (type == MessageDigest.ALG_SHA) {
            currentMDSpecs = new byte[]{MessageDigest.LENGTH_SHA, HMAC.SHA1_BLOCK_SIZE};
        } else if (type == MessageDigest.ALG_SHA_256) {
            currentMDSpecs = new byte[]{MessageDigest.LENGTH_SHA_256, HMAC.SHA256_BLOCK_SIZE};
        } else {
            ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED);
        }

        this.type = type;
    }

    @Override
    public void setKey(byte[] keyData, short kOff, short kLen) throws CryptoException, NullPointerException, ArrayIndexOutOfBoundsException {
        if (keyData == null)
            throw new NullPointerException();
        if (kOff < 0 || keyData.length <= kOff)
            throw new ArrayIndexOutOfBoundsException();
        if (kLen == 0 || keyData.length < (short) (kOff + kLen))
            throw new CryptoException(CryptoException.ILLEGAL_VALUE);

        if (kLen > currentMDSpecs[1]) {
            // Reduce key size through hashing it
            byte[] tempKey = new byte[kLen];
            MessageDigest mdEngine = MessageDigest.getInstance(this.type, false);
            JCSystem.beginTransaction();
            this.key = new byte[mdEngine.getLength()];
            mdEngine.doFinal(tempKey, (short) 0, kLen, this.key, mdEngine.getAlgorithm());
        } else {
            JCSystem.beginTransaction();
            this.key = new byte[kLen];
            Util.arrayCopy(keyData, kOff, this.key, (short) 0, kLen);
        }
        initialized_flag = true;
        JCSystem.commitTransaction();
    }

    @Override
    public byte getKey(byte[] keyData, short kOff) throws CryptoException {
        if (!this.initialized_flag)
            throw new CryptoException(CryptoException.UNINITIALIZED_KEY);

        Util.arrayCopy(this.key, (short) 0, keyData, kOff, (short) this.key.length);
        if (this.key.length > 127)
            return -1;
        else
            return (byte) this.key.length;
    }

    @Override
    public boolean isInitialized() {
        return initialized_flag;
    }

    @Override
    public void clearKey() {
        JCSystem.beginTransaction();
        this.initialized_flag = false;
        this.key = null;
        JCSystem.commitTransaction();
    }

    @Override
    public byte getType() {
        return this.type;
    }

    @Override
    public short getSize() {
        return (short) (this.key.length * 8);
    }
}
