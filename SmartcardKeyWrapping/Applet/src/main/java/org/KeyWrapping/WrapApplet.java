package org.KeyWrapping;

import javacard.framework.*;
import javacard.security.AESKey;
import javacard.security.CryptoException;
import javacard.security.KeyBuilder;
import javacard.security.MessageDigest;
import javacardx.apdu.ExtendedLength;
import javacardx.crypto.Cipher;

public class WrapApplet extends Applet implements ExtendedLength {

    private static final byte KEY_LENGTH    =   (byte) 0x20;
    private static final byte TAG_LENGTH    =   (byte) 0x20;
    private static final byte LEN_LENGTH    =   (byte) 0x02;
    private static final byte WRAP_CLA      =   (byte) 0xa0;
    private static final byte KEYINSERT_INS =   (byte) 0x01;
    private static final byte UNWRAP_INS    =   (byte) 0x02;
    private static final byte WRAP_INS      =   (byte) 0x03;
    private static final byte AES_KEY       =   (byte) 0x01;
    private static final byte HMAC_KEY      =   (byte) 0x02;
    public static final short MAX_WKC_SIZE  =   (short) 1024;


    private AESKey serverAESKey = null;
    private HMACKey serverHMACKey = null;

    private Cipher aesEngine = null;
    private HMAC hmacEngine = null;

    private byte[] dataBuffer = null;

    public static void install(byte[] bArray, short bOffset, byte bLength) {
        new WrapApplet(bArray, bOffset, bLength);
    }

    public WrapApplet(byte[] bArray, short bOffset, byte bLength) {
        serverAESKey = (AESKey) KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
        serverHMACKey = new HMACKey(MessageDigest.ALG_SHA_256);
        register();
        aesEngine = new AES_CTR();
        hmacEngine = new HMAC(MessageDigest.ALG_SHA_256);
        dataBuffer = JCSystem.makeTransientByteArray(MAX_WKC_SIZE, JCSystem.CLEAR_ON_DESELECT);
    }

    @Override
    public void process(APDU apdu) throws ISOException {
        byte[] buf = apdu.getBuffer();

        if ((buf[ISO7816.OFFSET_CLA] == 0) && (buf[ISO7816.OFFSET_INS] == (byte) 0xa4)) {
            return;
        }

        if (buf[ISO7816.OFFSET_CLA] != WRAP_CLA) {
            ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
        }

        byte ins = buf[ISO7816.OFFSET_INS];

        switch (ins) {
            case KEYINSERT_INS:
                insertKey(apdu);
                break;
            case UNWRAP_INS:
                unwrapKey(apdu);
                break;
            case WRAP_INS:
                wrapKey(apdu);
                break;
            default:
                ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
    }

    private short readIncomingDataIntoBuffer(APDU apdu, short bufferOffset) {
        short readCount = apdu.setIncomingAndReceive();
        byte[] apduBuffer = apdu.getBuffer();
        short offsetCData = apdu.getOffsetCdata();
        short bytesLeft = apdu.getIncomingLength();
        short bufferPosition = bufferOffset;
        while (bytesLeft > 0) {
            Util.arrayCopyNonAtomic(apduBuffer, offsetCData, dataBuffer, bufferPosition, readCount);
            bytesLeft -= readCount;
            bufferPosition += readCount;
            readCount = apdu.receiveBytes(offsetCData);
        }

        return (short) (bufferPosition - bufferOffset);
    }

    private void insertKey(APDU apdu) {
        short bytesRead = readIncomingDataIntoBuffer(apdu, (short) 0);
        byte[] buf = apdu.getBuffer();

        if (KEY_LENGTH != apdu.getIncomingLength() || bytesRead != KEY_LENGTH) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        }

        byte param1 = buf[ISO7816.OFFSET_P1];
        if (param1 == AES_KEY) {
            serverAESKey.setKey(dataBuffer, (short) 0);
        } else if (param1 == HMAC_KEY) {
            serverHMACKey.setKey(dataBuffer, (short) 0, KEY_LENGTH);
        }
    }

    /*
    private void read_key(APDU apdu) {
        short le = apdu.setOutgoing();
        apdu.setOutgoingLength(KEY_LENGTH);
        if (le != KEY_LENGTH) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        }
        byte param1 = apdu.getBuffer()[ISO7816.OFFSET_P1];
        if (param1 == AES_KEY) {
            serverAESKey.getKey(apdu.getBuffer(), (short) 0);
        } else if (param1 == HMAC_KEY) {
            serverHMACKey.getKey(apdu.getBuffer(), (short) 0);
        }
        apdu.sendBytes((short) 0, KEY_LENGTH);

    */
    /*
   ``len = len(WKc)`` (16 bit, network byte order)

   ``T = HMAC-SHA256(Ka, len || Kc || metadata)``

   ``IV = 128 most significant bits of T``

   ``WKc = T || AES-256-CTR(Ke, IV, Kc || metadata) || len``
 */
    private void unwrapKey(APDU apdu) {
        short bytesReceived = readIncomingDataIntoBuffer(apdu, (short) 0);

        short ciphertextLength = (short) (bytesReceived - TAG_LENGTH - LEN_LENGTH);

        short tagOff = 0;
        short wrapOff = (short) (tagOff + TAG_LENGTH);
        short lenOff = (short) (wrapOff + ciphertextLength);

        short wrappedKeyLength = Util.makeShort(dataBuffer[lenOff], dataBuffer[(short) (lenOff + 1)]);

        if (wrappedKeyLength != bytesReceived) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        }

        aesEngine.init(serverAESKey, Cipher.MODE_DECRYPT, dataBuffer, tagOff, AES_CTR.IV_SIZE);
        short bytesProcessed = aesEngine.doFinal(dataBuffer, wrapOff, ciphertextLength, dataBuffer, wrapOff);
        if (bytesProcessed != ciphertextLength) {
            throw new CryptoException(CryptoException.ILLEGAL_VALUE);
        }

        hmacEngine.init(serverHMACKey, MessageDigest.ALG_SHA_256);
        hmacEngine.update(dataBuffer, lenOff, LEN_LENGTH);
        hmacEngine.doFinal(dataBuffer, wrapOff, bytesProcessed, apdu.getBuffer(), apdu.getOffsetCdata());

        if (Util.arrayCompare(dataBuffer, tagOff, apdu.getBuffer(), apdu.getOffsetCdata(), TAG_LENGTH) != 0) {
            ISOException.throwIt(ISO7816.SW_DATA_INVALID);
        }

        short le = apdu.setOutgoing();

        if (le != ciphertextLength) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        }
        apdu.setOutgoingLength(ciphertextLength);

        apdu.sendBytesLong(dataBuffer, wrapOff, ciphertextLength);
    }

    private void wrapKey(APDU apdu) {
        short bytesReceived = readIncomingDataIntoBuffer(apdu, TAG_LENGTH);

        short tagOffset = 0;
        short keyOffset = (short) (tagOffset + TAG_LENGTH);
        short lenOffset = (short) (keyOffset + bytesReceived);

        short wrappedLength = (short) (bytesReceived + TAG_LENGTH + LEN_LENGTH);
        copyShortToArray(wrappedLength, dataBuffer, lenOffset);

        hmacEngine.init(serverHMACKey, MessageDigest.ALG_SHA_256);
        hmacEngine.update(dataBuffer, lenOffset, LEN_LENGTH);
        hmacEngine.doFinal(dataBuffer, keyOffset, bytesReceived, dataBuffer, tagOffset);

        aesEngine.init(serverAESKey, Cipher.MODE_ENCRYPT, dataBuffer, tagOffset, AES_CTR.IV_SIZE);
        aesEngine.doFinal(dataBuffer, keyOffset, bytesReceived, dataBuffer, keyOffset);

        short le = apdu.setOutgoing();

        if (le != wrappedLength) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        }
        apdu.setOutgoingLength(wrappedLength);

        apdu.sendBytesLong(dataBuffer, tagOffset, wrappedLength);

    }

    public static byte[] makeByteArray(short sNumber) {
        byte[] res = new byte[2];
        res[0] = (byte) ((sNumber >> 8) & 0xFF);
        res[1] = (byte) (sNumber & 0xFF);
        return res;
    }

    public static void copyShortToArray(short sNumber, byte[] array, short aOffset) {
        array[aOffset] = (byte) ((sNumber >> 8) & 0xFF);
        array[(short) (aOffset + 1)] = (byte) (sNumber & 0xFF);
    }

    public static void sendResponse(APDU apdu, byte[] dataBuffer, short wrapOff, short ciphertextLength) {
        byte[] buffer = new byte[ciphertextLength];
        Util.arrayCopyNonAtomic(dataBuffer, wrapOff, buffer, (short) 0, ciphertextLength);
        sendResponse(apdu, buffer);
    }

    public static void sendResponse(APDU apdu, byte[] response) {
        short le = apdu.setOutgoing();

        apdu.setOutgoingLength((short) response.length);

        apdu.sendBytesLong(response, (short) 0, (short) response.length);
    }
}