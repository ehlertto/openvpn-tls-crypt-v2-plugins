#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bits/stdint-uintn.h>

#include <stdbool.h>
#include <winscard.h>

#define SELECT_LIST 0x00, 0xA4, 0x04, 0x00
#define CHOOSE_LIST 0x08
#define UNCHOOSE_LIST 0x00
#define APP_ID_LIST 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
#define ARR(...)    \
    {               \
        __VA_ARGS__ \
    }

struct CardConnectionContext
{
    SCARDCONTEXT hContext;
    SCARDHANDLE hCard;
    const SCARD_IO_REQUEST *pioSendPci;
};

struct Request
{
    BYTE *buffer;
    DWORD length;
};

int connectToCard(struct CardConnectionContext *ctx);
int sendAPDU(struct CardConnectionContext *ctx, struct Request *sendBuffer, struct Request *receiveBuffer);
int disconnectFromCard(struct CardConnectionContext *ctx);
int selectApp(struct CardConnectionContext *ctx);
int unselectApp(struct CardConnectionContext *ctx);

long getFirstReader(struct CardConnectionContext *ctx, char **reader)
{
    LONG rv;
    DWORD dwReaders;
    LPSTR mszReaders = NULL;
    int nbReaders;
    char *ptr = NULL;

    // Get available readers
    dwReaders = SCARD_AUTOALLOCATE;
    rv = SCardListReaders(ctx->hContext, NULL, (LPSTR) &mszReaders, &dwReaders);
    if (rv != SCARD_S_SUCCESS)
    {
        puts("SCardListReaders");
        return false;
    }

    ptr = mszReaders;
    if (*ptr == '\0')
    {
        puts("No readers found! Please connect a card reader.");
        return false;
    }
    *reader = ptr;

    return true;
}

int connectToCard(struct CardConnectionContext *ctx)
{
    LONG rv;
    char *reader = NULL;
    DWORD dwActiveProtocol;

    rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &ctx->hContext);
    if (rv != SCARD_S_SUCCESS)
    {
        puts("SCardEstablishContext");
        return false;
    }

    rv = getFirstReader(ctx, &reader);
    if (! rv)
    {
        puts("getFirstReader() failed");
        if (reader)
        {
            SCardFreeMemory(ctx->hContext, reader);
        }
        return false;
    }

    // Connect to card
    dwActiveProtocol = -1;
    rv = SCardConnect(ctx->hContext, reader, SCARD_SHARE_SHARED,
                      SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &ctx->hCard, &dwActiveProtocol);
    SCardFreeMemory(ctx->hContext, reader);
    if (rv != SCARD_S_SUCCESS)
    {
        puts("Could not connect to smart card in card reader. Is the card inserted?");
        return false;
    }

    switch (dwActiveProtocol)
    {
        case SCARD_PROTOCOL_T0:
            ctx->pioSendPci = SCARD_PCI_T0;
            break;
        case SCARD_PROTOCOL_T1:
            ctx->pioSendPci = SCARD_PCI_T1;
            break;
        default:
            puts("Unknown protocol");
            return false;
    }

    return true;
}

void printRequest(struct Request *req, char title[])
{
    printf("%s", title);
    for (int i = 0; i < req->length; i++)
        printf("%02X ", req->buffer[i]);
    printf("\n");
}

int sendAPDU(struct CardConnectionContext *ctx, struct Request *sendBuffer, struct Request *receiveBuffer)
{
    SCARD_IO_REQUEST pioRecvPci;
    LONG rv;
    SCardBeginTransaction(ctx->hCard);
    // printRequest(sendBuffer, ">> ");
    rv = SCardTransmit(ctx->hCard, ctx->pioSendPci, sendBuffer->buffer, sendBuffer->length,
                       &pioRecvPci, receiveBuffer->buffer, &receiveBuffer->length);
    SCardEndTransaction(ctx->hCard, SCARD_LEAVE_CARD);
    // printRequest(receiveBuffer, "<<  ");
    return rv == SCARD_S_SUCCESS;
}

int disconnectFromCard(struct CardConnectionContext *ctx)
{
    if (SCardDisconnect(ctx->hCard, SCARD_RESET_CARD) != SCARD_S_SUCCESS)
    {
        puts("SCardDisconnect");
        return false;
    }
    return true;
}

int selectApp(struct CardConnectionContext *ctx)
{
    BYTE selectAPDU[] = ARR(SELECT_LIST, CHOOSE_LIST, APP_ID_LIST);

    struct Request selectRequest = {selectAPDU, sizeof(selectAPDU)};
    BYTE pbRecvBuffer[2];
    struct Request recvBuffer = {pbRecvBuffer, sizeof(pbRecvBuffer)};
    return sendAPDU(ctx, &selectRequest, &recvBuffer);
}

int unselectApp(struct CardConnectionContext *ctx)
{
    BYTE unselectAPDU[] = ARR(SELECT_LIST, UNCHOOSE_LIST);

    struct Request unselectRequest = {unselectAPDU, sizeof(unselectAPDU)};
    BYTE pbRecvBuffer[32];
    struct Request recvBuffer = {pbRecvBuffer, sizeof(pbRecvBuffer)};
    int status = sendAPDU(ctx, &unselectRequest, &recvBuffer);
    return status;
}

void releaseContext(struct CardConnectionContext *ctx)
{

    LONG rv = SCardReleaseContext(ctx->hContext);
    if (rv != SCARD_S_SUCCESS)
        printf("SCardReleaseContext: %s (0x%lX)\n", pcsc_stringify_error(rv), rv);
}
#ifdef DEBUG
int main(int argc, char *argv[])
{
    LONG rv;
    struct CardConnectionContext ctx;
    rv = connectToCard(&ctx);
    if (! rv)
    {
        goto end;
    }

    rv = selectApp(&ctx);
    if (! rv)
    {
        goto end;
    }

    uint8_t importAES[] = {0xA0, 0x01, 0x01, 0x00, 0x20, 0xBF, 0xFC, 0x61, 0xE8, 0x05, 0xC5, 0xD5, 0x37, 0x48, 0xD2, 0xF7, 0xD4, 0xAD, 0x90, 0x6A, 0x88, 0x0B, 0x91, 0x95, 0x15, 0xE6, 0x73, 0xB9, 0x14, 0x16, 0x92, 0x48, 0xE6, 0xF6, 0xD8, 0x17, 0x02};
    struct Request importAESRequest = {importAES, sizeof(importAES)};
    BYTE pbRecvBuffer[2];
    struct Request recvBuffer = {pbRecvBuffer, sizeof(pbRecvBuffer)};
    sendAPDU(&ctx, &importAESRequest, &recvBuffer);

    uint8_t importHMAC[] = {0xA0, 0x01, 0x02, 0x00, 0x20, 0xEE, 0xFD, 0x70, 0x51, 0x6B, 0x71, 0x10, 0x45, 0x0A, 0x8D, 0x48, 0x5B, 0x08, 0x05, 0x37, 0xC1, 0x76, 0x08, 0x2B, 0x79, 0xBB, 0x77, 0xEC, 0x4D, 0x14, 0x6C, 0xF9, 0x7D, 0x13, 0xF4, 0x07, 0x4E};
    struct Request importHMACRequest = {importHMAC, sizeof(importHMAC)};
    sendAPDU(&ctx, &importHMACRequest, &recvBuffer);

    uint8_t unwrapKey[] = {0xA0, 0x02, 0x00, 0x00, 0x00, 0x01, 0x2B, 0x51, 0x0B, 0x79, 0x15, 0xA6, 0x87, 0x73, 0xFA, 0xC1, 0x6C, 0x3C, 0x15, 0x13, 0x02, 0xC8, 0xE3, 0x45, 0xA9, 0xC6, 0x6D, 0xDF, 0xDD, 0xAF, 0xEC, 0x4D, 0xD5, 0xE2, 0xD4, 0x94, 0x61, 0xCD, 0xC9, 0xBB, 0x2B, 0x8B, 0x0A, 0x0A, 0x33, 0xF3, 0x4B, 0x89, 0xA6, 0x7D, 0x4D, 0x5E, 0x03, 0x66, 0x8F, 0xA3, 0xE6, 0xC8, 0x08, 0xFD, 0x06, 0x1F, 0x21, 0xE1, 0xCC, 0x6D, 0x67, 0x90, 0x1B, 0x62, 0x74, 0xE1, 0x42, 0xB4, 0xE2, 0xA0, 0xB3, 0xB7, 0xF9, 0x4C, 0x84, 0xD5, 0x22, 0x0B, 0x8F, 0xEB, 0x39, 0x82, 0x21, 0x9B, 0xB3, 0x97, 0x89, 0x50, 0xE8, 0x30, 0x68, 0x56, 0xA2, 0x3C, 0xE3, 0xD8, 0x3B, 0xCA, 0x51, 0x06, 0x7A, 0xDF, 0x5A, 0x65, 0x69, 0xCD, 0x91, 0x36, 0x50, 0x3A, 0xE9, 0xCE, 0xAE, 0x05, 0x2E, 0xC2, 0xA9, 0xBA, 0xC0, 0x0D, 0xFD, 0x0A, 0x02, 0x2C, 0x4D, 0x22, 0xCB, 0x09, 0xAF, 0xB5, 0x19, 0x0D, 0x23, 0x34, 0xC4, 0x59, 0xEF, 0xB5, 0x2A, 0x1C, 0x90, 0xA7, 0xE3, 0x0E, 0x72, 0x87, 0x01, 0xA9, 0x1F, 0xAB, 0x99, 0x4A, 0x41, 0xF3, 0xBD, 0x40, 0x8D, 0xFA, 0xCC, 0x7D, 0x18, 0x0A, 0x49, 0x48, 0x5F, 0xDA, 0xE0, 0x21, 0x64, 0x34, 0xCD, 0x4A, 0x8F, 0x4F, 0x57, 0x9D, 0x32, 0xE7, 0x86, 0x86, 0xC7, 0x13, 0x7A, 0x8D, 0xA6, 0x87, 0x57, 0xA0, 0x42, 0x39, 0x28, 0x4E, 0x85, 0x5E, 0x77, 0x5C, 0xEF, 0x74, 0xE2, 0x7D, 0xF8, 0x67, 0xBA, 0x78, 0x1C, 0x70, 0xBF, 0x1A, 0x6A, 0xB3, 0xBD, 0x92, 0x76, 0x2A, 0x13, 0x08, 0x19, 0x98, 0xD8, 0xEE, 0x73, 0xDE, 0xBC, 0x20, 0xF8, 0xD8, 0x1E, 0x5A, 0xAD, 0x67, 0xA7, 0x5C, 0xCA, 0xFB, 0xA7, 0x7B, 0xCC, 0x3F, 0x57, 0xE7, 0x3F, 0x42, 0xC3, 0x86, 0xD0, 0xC8, 0x22, 0xCA, 0x30, 0x1A, 0x3D, 0xBE, 0x98, 0xCD, 0x70, 0x34, 0x40, 0x4F, 0x36, 0x91, 0x76, 0xFC, 0x69, 0xF5, 0x88, 0x49, 0x87, 0xE1, 0x19, 0x3A, 0xE7, 0x83, 0xDB, 0xE7, 0xEC, 0x39, 0x21, 0x00, 0xF4, 0x80, 0x7F, 0x5B, 0x2B, 0xE6, 0x6A, 0x0D, 0x86, 0x5D, 0x45, 0x24, 0xA0, 0x5C, 0xBE, 0xE1, 0x4C, 0xF2, 0x8E, 0x1A, 0x01, 0x2B, 0x01, 0x09};
    struct Request unwrapKeyRequest = {unwrapKey, sizeof(unwrapKey)};
    BYTE keyBuffer[1024];
    struct Request keyBufferRequest = {keyBuffer, sizeof(keyBuffer)};
    sendAPDU(&ctx, &unwrapKeyRequest, &keyBufferRequest);

    if (keyBufferRequest.buffer[keyBufferRequest.length - 2] != 0x90 || keyBufferRequest.buffer[keyBufferRequest.length - 1] != 0x00)
    {
        puts("Unwrapping on smartcard failed");
    }
    puts("Unwrapping worked!");

    rv = unselectApp(&ctx);
    if (! rv)
    {
        goto end;
    }

    rv = disconnectFromCard(&ctx);
    if (! rv)
    {
        goto end;
    }
end:
    releaseContext(&ctx);
    return EXIT_SUCCESS;
}
#endif
