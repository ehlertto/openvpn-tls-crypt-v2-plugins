#include <iostream>
#include <dlfcn.h>
#include <openssl/evp.h>
#include <vector>
#include <chrono>
#include <cmath>
#include <csignal>
#include <fstream>
#include "openvpn-plugin.h"


#define STRUCT_VERSION 5

#define SMARTCARD_KEY_WRAPPING_PATH     "./plugins/libSmartcardKeyWrappingLibrary.so"
#define SMARTCARD_KEY_DERIVATION_PATH   "./plugins/libSmartcardKeyDerivationLibrary.so"
#define YUBIKEY_PATH                    "./plugins/libYubikeyKeyManagementLib.so"
#define SOFTHSM_PATH                    "./plugins/libPKCS11KeyWrappingLibrary.so"

#define SOFTHSM_WKC "5b4QibA+L9KnvuwC4yMK8hcDwhQFyVr7y4Tn/v0WfEMh5lPUOnIysitKvbNo1PcuY1sT4uwiAb9OAICWdbMlTP2KibR/H0xrg/H4GvNiSk6KHVF3xNGY3n72CK+yKJdXCkF2GUBA0bEaZhxQKylfUoAdHcqBiadzJWeFqUuxty6Tni0Owaf3asrDhiG3Zo9oO3GyUiC0VM+K28lQYtUqP3tjA8DgODoN7fsz5JTt8JcCk8KNjmxLV4fJL8+i16+Eh56i0xQfou2lCpS1UIYaaCjqRgkvlptcDtDmM94p18PKHH19vftSvochfyeFEnRHAYD/V5JwU2LWi+IdAW8wls4jf0Voy66bGSTENtsZ//HA+tvBbXJ9OIqu+jMGYc/J3IEYlb3ZmfoxASs="
#define YUBIKEY_WKC "yiViOmA43UGJer+VMWcTS/BFlOqZ/MAO1BJedK6xjYZt9fo71H2N8EcvQlGeK4IP9ZfwQ4kn2tA+HTn9UmN3zS+XOkBx1XbLWUASHeAXS/w0GGQAq9Wj0Ni7+crp4RH/k3/3BOTn/x1tjlYI0XPZFd8R8W8paxuxwFyHS7ISp2NYMLkFtVCCnv+1Qwg76uGkCCPI7RKUFsOzYUpHaF3/IpRhexpWZW5Zm+Xvsnc/UH3j5mYamE81f7+BKjPtx2vrtufl4i8dnar0+rpfA+9mTZboGi93EwDpgqM4qVoJJ4lq2YeKWH5Fh5N3dKw/6yEHIAasfQN685xmZ6d3Hg8XwDVzTneTPTjSbbB+d+WWjGMyNEx7whgB4irg1SgcCP9Zf0cY11PIofHAASs="
#define SMARTCARD_KEY_WRAPPING_WKC "9Nap/uYnVzz/tFZHX+z78Fc28WuMhljAlWCP22dcbBpRrAsX7/kLTOE20cgPeGJLXgappBV42NzL0TFKY+oJXTC1/GfhDfVwIlY1oMzKyOHtHIWqvs/G7+AEMv2GDZCl9Pt2pTGS/9WnBSmSqnOavCrt5KNrUb3T0X3EXNgyfCKdovhGQ4pdwEkrvsXiAR3bLeY66jo+TDcdj7FVcMoCfHeIvjkoo7MiJu0mZ7hpuLo4E8j9ClObvbqYvBDyjuH2S250pANCxykuW/tk+rNcVdTKzNpJtBUvvH3YDAbGOTtcHvK3txeLyy7e3SYzLxBsoOEVMk1FMcXODjiLyTw8Y3eX7DfAk92vcqR6eXv9gDCLZhCGyUOaZh/aBHliOP2Twzb3FUhE5sgAASs="
#define SMARTCARD_KEY_DERIVATION_WKC "KW9RjSfhyM6JBgfvs7/SDcFD9eFwO/5FiDOChWE1PZ31pfNhhYl5Iytg7Y6VDGuWNyhYq47koVuXdLSpva5qNSt1uRcqaY1nNbWwBMCgSIjcNJb3+t0FxtbZeYP4w78v18Ad8j3Qf2e3T2NN4VvEebffsihPf0vlvqGaRJvn7W8NXh3Y28/vPb4tXLSalpR5+lZrXHLwn+pl03sH3+nSYIO4Fk+DbSA0aVTKXDulVtNYugMy2P9XqQRtlM1lOuMBqxbyc2FB7oqFvrLTHVZnWqGFimvwasIaktKfkxXT+X1uaoUYzsFNbvs8s/Xul+9NF4YXMZpffMbA5ah5629zG8HW9L1tI9crd5GOKdH5QSThtSZinnSXhmhYCvAzhNdjDQc/oCpIicpeASs="

#define NS_PER_SECOND 1000000000
extern "C"
{
    #include "base64.h"
}

static std::vector<float> cipher_duration;
static std::vector<float> auth_duration;

void vlog_func(openvpn_plugin_log_flags_t flags, const char *plugin_name, const char *format, va_list arglist) {
    (void)plugin_name;
    if(strcmp(format, "cipher_duration: %f") == 0)
    {
        float duration = va_arg(arglist, double);
        cipher_duration.push_back(duration);
    }
    else if (strcmp(format, "auth_duration: %f") == 0)
    {
        float duration = va_arg(arglist, double);
        auth_duration.push_back(duration);
    }
    else
    {
        vprintf(format, arglist);
        puts("");
    }
}

void plugin_secure_memzero(void *data, size_t len) {
    (void) data;
    (void) len;

    // Not Implemented
}

void plugin_log(openvpn_plugin_log_flags_t flags, const char *plugin_name, const char *format, ...) {
    (void) flags;
    (void) plugin_name;
    (void) format;

    // Not Implemented
}

std::string bytes_to_hex(const unsigned char *data, int size) {
    char const hex_chars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    std::string hex;
    for( int i = 0; i < size; i++) {
        unsigned char byte = data[i];
        hex.push_back(hex_chars[ ( byte & 0xF0 ) >> 4 ]);
        hex.push_back(hex_chars[ ( byte & 0x0F ) >> 0 ]);
    }
    return hex;
}

class Plugin
{
    void *plugin_lib;
    int (*openvpn_plugin_open_v3)(const int v3structver,
                                  struct openvpn_plugin_args_open_in const *args,
                                  struct openvpn_plugin_args_open_return *ret);
    int (*openvpn_plugin_func_v3)(const int version,
                                  struct openvpn_plugin_args_func_in const *arguments,
                                  struct openvpn_plugin_args_func_return *retptr);
    openvpn_plugin_handle_t handle{};
public:
    explicit Plugin(const std::string &plugin_path);
    ~Plugin();
    void open_plugin(int num, ...);
    std::string execute_plugin(int call_type, int num, ...);

    int up_plugin();
};

Plugin::Plugin(const std::string &plugin_path)
{
    this->plugin_lib = dlopen(plugin_path.c_str(), RTLD_LAZY);

    if (!this->plugin_lib) {
        std::cerr << dlerror() << std::endl;
        throw std::invalid_argument("Couldn't load plugin library");
    }

    this->openvpn_plugin_open_v3 = reinterpret_cast<int (*)(const int v3structver, struct openvpn_plugin_args_open_in const *args, struct openvpn_plugin_args_open_return *ret)>
                                     (
                                             dlsym(this->plugin_lib, "openvpn_plugin_open_v3")
                                     );
    this->openvpn_plugin_func_v3 = reinterpret_cast<int (*)(const int version, struct openvpn_plugin_args_func_in const *arguments, struct openvpn_plugin_args_func_return *retptr)>
                                     (
                                             dlsym(this->plugin_lib, "openvpn_plugin_func_v3")
                                     );

    char *error;
    if ((error = dlerror()) != nullptr) {
        std::cerr << error << std::endl;
        throw std::exception();
    }
}

void free_stringlist(openvpn_plugin_string_list **string_list)
{
    auto next = *string_list;
    while (next)
    {
        auto current = next;
        next = current->next;
        free(current);
    }
    free(string_list);
}

void Plugin::open_plugin(int num, ...) {
    va_list valist;
    va_start(valist, num);
    const char **argv_open = static_cast<const char **>(
                             calloc(num, sizeof(char *)));
    for (int i = 0; i < num; i++)
    {
        argv_open[i+1] = va_arg(valist, char *);
    }

    openvpn_plugin_callbacks callbacks = {
            nullptr,
            vlog_func,
            plugin_secure_memzero,
            openvpn_base64_encode,
            openvpn_base64_decode,
    };

    struct openvpn_plugin_args_open_in args_open =
            {
                    0,
                    reinterpret_cast<const char **const>(argv_open),
                    nullptr,
                    &callbacks,
                    SSLAPI_OPENSSL,
                    nullptr,
                    0,
                    0,
                    nullptr
            };

    struct openvpn_plugin_args_open_return ret_open{};
    auto retlist = static_cast<openvpn_plugin_string_list **>(
            calloc(1,sizeof(openvpn_plugin_string_list *)));
    ret_open.return_list = retlist;

    openvpn_plugin_open_v3(STRUCT_VERSION, &args_open, &ret_open);
    this->handle = ret_open.handle;
    if (! this->handle)
        throw std::invalid_argument("");

    free_stringlist(ret_open.return_list);
    ret_open.return_list = nullptr;
}

std::string Plugin::execute_plugin(int call_type, int num, ...) {
    va_list valist;
    va_start(valist, num);
    const char **argv_func = static_cast<const char **>(
            calloc(num, sizeof(char *)));
    for (int i = 0; i < num; i++)
    {
        argv_func[i+1] = va_arg(valist, char *);
    }

    struct openvpn_plugin_args_func_in args_func = {
            call_type,
            argv_func,
            nullptr,
            this->handle,
            nullptr,
            0,
            nullptr
    };

    struct openvpn_plugin_args_func_return ret_func{};

    auto retlist = static_cast<openvpn_plugin_string_list **>(
                                          calloc(1,sizeof(openvpn_plugin_string_list *)));
    ret_func.return_list = retlist;

    int ret_code = openvpn_plugin_func_v3(STRUCT_VERSION, &args_func, &ret_func);

    if(ret_code != OPENVPN_PLUGIN_FUNC_SUCCESS)
        return "";
    char * return_base64 = (*ret_func.return_list)->value;
    unsigned char return_bytes[strlen(return_base64)];
    int byte_len = openvpn_base64_decode(return_base64, return_bytes, (int) sizeof(return_bytes));
    std::string return_hex = bytes_to_hex(return_bytes, byte_len);

    free_stringlist(ret_func.return_list);
    ret_func.return_list = nullptr;

    return return_hex;
}

int Plugin::up_plugin() {
    struct openvpn_plugin_args_func_in args_func = {
            OPENVPN_PLUGIN_UP,
            nullptr,
            nullptr,
            this->handle,
            nullptr,
            0,
            nullptr
    };

    struct openvpn_plugin_args_func_return ret_func{};

    auto retlist = static_cast<openvpn_plugin_string_list **>(
            calloc(1,sizeof(openvpn_plugin_string_list *)));
    ret_func.return_list = retlist;

    int ret_code = openvpn_plugin_func_v3(STRUCT_VERSION, &args_func, &ret_func);

    free_stringlist(ret_func.return_list);
    ret_func.return_list = nullptr;

    return ret_code;
}

Plugin::~Plugin() {
    dlclose(this->plugin_lib);
}

enum plugin_types {
    SMARTCARD_KEY_WRAPPING,
    SMARTCARD_KEY_DERIVATION,
    YUBIKEY,
    SOFTHSM
};

Plugin createPlugin(int type) {
    Plugin *plugin = nullptr;

    switch(type)
    {
        case SMARTCARD_KEY_WRAPPING:
            plugin = new Plugin(SMARTCARD_KEY_WRAPPING_PATH);
            plugin->open_plugin(0);
            break;
        case YUBIKEY:
            plugin = new Plugin(YUBIKEY_PATH);
            // Slot: 2; Access_Code: 0
            plugin->open_plugin(2 , "3", "0");
            if (plugin->up_plugin() != OPENVPN_PLUGIN_FUNC_SUCCESS)
                throw std::exception();
            break;
        case SOFTHSM:
            plugin = new Plugin(SOFTHSM_PATH);
            plugin->open_plugin(2 , "/home/triton/Development/sc-hsm-embedded/src/pkcs11/.libs/libsc-hsm-pkcs11.so", "123456");
            break;
        default:
            throw std::invalid_argument("Plugin type not implemented!");
    }

    return *plugin;
}

void sub_timespec(struct timespec t1, struct timespec t2, struct timespec *td)
{
    td->tv_nsec = t2.tv_nsec - t1.tv_nsec;
    td->tv_sec  = t2.tv_sec - t1.tv_sec;
    if (td->tv_sec > 0 && td->tv_nsec < 0)
    {
        td->tv_nsec += NS_PER_SECOND;
        td->tv_sec--;
    }
    else if (td->tv_sec < 0 && td->tv_nsec > 0)
    {
        td->tv_nsec -= NS_PER_SECOND;
        td->tv_sec++;
    }
}

float calculate_standard_deviation(const std::vector<float>& values)
{
    float sum = 0;
    for (float e : values)
    {
        sum += e;
    }

    float average = sum / (float) values.size();

    // Standard Deviation
    float standard_deviation = 0;
    for (float e : values)
    {
        standard_deviation += powf((e - average), 2.f);
    }

    standard_deviation = sqrtf(standard_deviation / (float) (values.size() - 1));

    return standard_deviation;
}

int main() {
    Plugin plugin = createPlugin(YUBIKEY);

    std::string WKc = YUBIKEY_WKC;
    timespec start_c{}, end_c{}, diff_c{};
    clock_gettime(CLOCK_MONOTONIC, &start_c);
    clock_gettime(CLOCK_MONOTONIC, &end_c);
    sub_timespec(start_c, end_c, &diff_c);
    float duration_c = (float) diff_c.tv_sec * 1000. + (float) diff_c.tv_nsec / 1000000.;

    setuid(65534);
    setgid(65534);

    int iterations = 100;
    std::vector<float> duration_lists;
    for (int i = 0; i < iterations; i++) {
        auto start = std::chrono::steady_clock::now();
        plugin.execute_plugin(OPENVPN_PLUGIN_CLIENT_KEY_WRAPPING, 2, "unwrap", WKc.c_str());
        auto end = std::chrono::steady_clock::now();
        float duration_ms = (
                static_cast<float>(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()) /
                1000000);
        duration_lists.push_back(duration_ms);

        std::cout << "\r" << i + 1 << " / " << iterations << " completed";
        fflush(stdout);
        sleep(1);
    }

    float sum = 0;
    for (float e: duration_lists) {
        sum += e;
    }

    float average = sum / (float) iterations;
    float standard_deviation = calculate_standard_deviation(duration_lists);

    std::cout << std::endl;
    std::cout << sum << "ms total, average : " << average << "ms. With std dev of " << standard_deviation << std::endl;

    float cipher_sum = 0;
    for (auto e : cipher_duration) {
        cipher_sum += e;
    }
    float cipher_mean = cipher_sum / cipher_duration.size();
    float cipher_deviation = calculate_standard_deviation(cipher_duration);
    std::cout << cipher_sum << "ms cipher total, average : " << cipher_mean << "ms. With std dev of " << cipher_deviation << std::endl;

    float auth_sum = 0;
    for (auto e : auth_duration) {
        auth_sum += e;
    }
    float auth_mean = auth_sum / auth_duration.size();
    float auth_deviation = calculate_standard_deviation(auth_duration);
    std::cout << auth_sum << "ms auth total, average : " << auth_mean << "ms. With std dev of " << auth_deviation << std::endl;

    std::ofstream results_file("results.csv");
    results_file << "cipher, auth, total" << std::endl;
    auto cipher_iter = cipher_duration.cbegin();
    auto auth_iter = auth_duration.cbegin();
    auto total_iter = duration_lists.cbegin();
    while (cipher_iter != cipher_duration.end() || auth_iter != auth_duration.end() || total_iter != duration_lists.end())
    {
        if (cipher_iter != cipher_duration.end())
        {
            results_file << *cipher_iter;
            cipher_iter++;
        }
        results_file << ",";
        if (auth_iter != auth_duration.end())
        {
            results_file << *auth_iter;
            auth_iter++;
        }
        results_file << ",";
        if (total_iter != duration_lists.end())
        {
            results_file << *total_iter;
            total_iter++;
        }
        results_file << std::endl;
    }

    results_file.close();


    // std::string result = plugin.execute_plugin(OPENVPN_PLUGIN_CLIENT_KEY_WRAPPING, 2, "unwrap", WKc.c_str());
    /*
    if(result != "1298789D57618EC2FEF4936C6DD96CB56BCDF804816E02286F14B4156D13BED63279101A4FFAEE654A3FE6F990428061A1F9DE83ABB1B1347B865BC3A68A4ED57446BC0B6206B45640C50A3CA776DA195FD38126A471EBE296E1F77BB4581EA605DA37EDF4946843B7ABAD840A08EB1F3C53F818D6C4D1664CF7A14412ED79D329ED22B66D7771350397B4EE8B6D9D94E666809D8B8C5E7ACF1E536D78DC07ABA147E73FDA4F0E527FD87B8B28CF425047B128890975F156EC48237D8D4BCEA8EA361573BC187B61984764344AA5C778972E249F8AA08097C0FCE9DB0C69C1A36A01BF71C73E3425B9B66F241990A423DCFF9642CD97E11D4B90A2E7CF0732BF010000000063920AA4")
        throw std::exception();
    */
    return EXIT_SUCCESS;
}
