#include "openvpn-plugin.h"
#include <cmath>
#include <dlfcn.h>
#include <iostream>
#include <memory>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <vector>

extern "C"
{
#include "base64.h"
}

#define STRUCT_VERSION 5

class Plugin
{
    void *plugin_lib;
    int (*openvpn_plugin_open_v3)(const int v3structver,
                                  struct openvpn_plugin_args_open_in const *args,
                                  struct openvpn_plugin_args_open_return *ret);
    int (*openvpn_plugin_func_v3)(const int version,
                                  struct openvpn_plugin_args_func_in const *arguments,
                                  struct openvpn_plugin_args_func_return *retptr);
    openvpn_plugin_handle_t handle{};

  public:
    explicit Plugin(const std::string &plugin_path);
    ~Plugin();
    void open_plugin(int argc, const char **argv);
    void execute_plugin(int call_type, int num, ...);
};

void vlog_func(openvpn_plugin_log_flags_t flags, const char *plugin_name, const char *format, va_list arglist)
{
    (void) plugin_name;
    vprintf(format, arglist);
    puts("");
}

void plugin_secure_memzero(void *data, size_t len)
{
    (void) data;
    (void) len;

    // Not Implemented
}

void plugin_log(openvpn_plugin_log_flags_t flags, const char *plugin_name, const char *format, ...)
{
    (void) flags;
    (void) plugin_name;
    (void) format;

    // Not Implemented
}

Plugin::Plugin(const std::string &plugin_path)
{
    this->plugin_lib = dlopen(plugin_path.c_str(), RTLD_LAZY);

    if (! this->plugin_lib)
    {
        std::cerr << dlerror() << std::endl;
        throw std::invalid_argument("Couldn't load plugin library");
    }

    this->openvpn_plugin_open_v3 = reinterpret_cast<int (*)(const int v3structver, struct openvpn_plugin_args_open_in const *args, struct openvpn_plugin_args_open_return *ret)>(
        dlsym(this->plugin_lib, "openvpn_plugin_open_v3"));
    this->openvpn_plugin_func_v3 = reinterpret_cast<int (*)(const int version, struct openvpn_plugin_args_func_in const *arguments, struct openvpn_plugin_args_func_return *retptr)>(
        dlsym(this->plugin_lib, "openvpn_plugin_func_v3"));

    char *error;
    if ((error = dlerror()) != nullptr)
    {
        std::cerr << error << std::endl;
        throw std::exception();
    }
}

void free_stringlist(openvpn_plugin_string_list **string_list)
{
    auto next = *string_list;
    while (next)
    {
        auto current = next;
        next = current->next;
        free(current);
    }
    free(string_list);
}

void Plugin::open_plugin(int argc, const char **argv)
{
    openvpn_plugin_callbacks callbacks = {
        plugin_log,
        vlog_func,
        plugin_secure_memzero,
        openvpn_base64_encode,
        openvpn_base64_decode,
    };

    struct openvpn_plugin_args_open_in args_open = {
        0,
        argv,
        nullptr,
        &callbacks,
        SSLAPI_OPENSSL,
        nullptr,
        0,
        0,
        nullptr,
    };

    struct openvpn_plugin_args_open_return ret_open
    {
    };
    auto retlist = static_cast<openvpn_plugin_string_list **>(
        calloc(1, sizeof(openvpn_plugin_string_list *)));
    ret_open.return_list = retlist;

    openvpn_plugin_open_v3(STRUCT_VERSION, &args_open, &ret_open);
    this->handle = ret_open.handle;
    if (! this->handle)
        throw std::invalid_argument("");

    free_stringlist(ret_open.return_list);
    ret_open.return_list = nullptr;
}

void Plugin::execute_plugin(int call_type, int num, ...)
{
    va_list valist;
    va_start(valist, num);
    const char *argv_func[num + 1];
    for (int i = 0; i < num; i++)
    {
        argv_func[i + 1] = va_arg(valist, char *);
    }

    struct openvpn_plugin_args_func_in args_func = {
        call_type,
        argv_func,
        nullptr,
        this->handle,
        nullptr,
        0,
        nullptr,
    };

    int ret_code = openvpn_plugin_func_v3(STRUCT_VERSION, &args_func, nullptr);

    if (ret_code != OPENVPN_PLUGIN_FUNC_SUCCESS)
        throw std::exception();
}

Plugin::~Plugin()
{
    dlclose(this->plugin_lib);
}

Plugin createPlugin(int argc, const char **argv)
{
    Plugin plugin(argv[0]);
    plugin.open_plugin(argc, argv);

    return plugin;
}

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        puts("USAGE: ./ImportKeyFile <key_file_path> <plugin_path> [plugin_specific_arguments]");
        return EXIT_FAILURE;
    }
    std::vector<const char *> const_args(argc);
    for (int i = 0; i < argc; ++i)
    {
        const_args[i] = argv[i + 2];
    }

    auto plugin = createPlugin(argc, const_args.data());

    char *key_file_path = argv[1];
    FILE *key_file = fopen(key_file_path, "r");
    char *name, *header;
    unsigned char *key_data;
    long len;
    int ret = PEM_read(key_file, &name, &header, &key_data, &len);
    if (! ret || len != 128)
    {
        puts("Error reading key file");
    }

    char *aes_key;
    char *hmac_key;
    openvpn_base64_encode(key_data, 64, &aes_key);
    openvpn_base64_encode(key_data + 64, 64, &hmac_key);
    OPENSSL_free(name);
    OPENSSL_free(header);
    OPENSSL_free(key_data);

    plugin.execute_plugin(OPENVPN_PLUGIN_CLIENT_KEY_WRAPPING, 3, "import", aes_key, hmac_key);
    free(aes_key);
    free(hmac_key);
    return 0;
}
