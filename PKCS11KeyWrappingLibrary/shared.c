#include "shared.h"

char *MODULE = "OPENVPN_PLUGIN_CLIENT_KEY_WRAPPING";

plugin_vlog_t plugin_vlog_func = NULL;
plugin_base64_decode_t ovpn_base64_decode = NULL;
plugin_base64_encode_t ovpn_base64_encode = NULL;
plugin_secure_memzero_t ovpn_secure_memzero = NULL;

void plog(int flags, char *fmt, ...)
{
    char logid[129];

    snprintf(logid, 128, "%s", MODULE);

    va_list arglist;
    va_start(arglist, fmt);
    if (plugin_vlog_func)
        plugin_vlog_func((openvpn_plugin_log_flags_t) flags, logid, fmt, arglist);
    else
    {
        vprintf(fmt, arglist);
        puts("");
    }
    va_end(arglist);
}