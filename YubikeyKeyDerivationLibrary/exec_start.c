#include "openvpn_yubikey_key_management.c"

static void
display_info(YK_KEY *yk, YK_STATUS *st)
{
    printf("Firmware version %d.%d.%d Touch level %d ",
           ykds_version_major(st),
           ykds_version_minor(st),
           ykds_version_build(st),
           ykds_touch_level(st));
    if (ykds_pgm_seq(st))
        printf("Program sequence %d\n", ykds_pgm_seq(st));
    else
        printf("Unconfigured\n");

    if ((ykds_version_major(st) > 2 ||
         (ykds_version_major(st) == 2 &&
          ykds_version_minor(st) >= 2) ||
         (ykds_version_major(st) == 2 && // neo has serial functions
          ykds_version_minor(st) == 1 &&
          ykds_version_build(st) >= 4)))
    {
        unsigned int serial;
        if (! yk_get_serial(yk, 0, 0, &serial))
        {
            printf("Failed to read serial number (serial-api-visible disabled?).\n");
        }
        else
        {
            printf("Serial number : %i\n", serial);
        }
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    clock_t start_t, end_t;

    YK_KEY *yk = 0;
    YK_STATUS *st = ykds_alloc();
    /* Assume the worst */
    int error = 1;
    int exit_code = 0;

    int key_index = 0;
    int verbose = 1;

    if (! key_init(&yk, st))
    {
        goto err;
    }

    error = 0;
    exit_code = 0;
err:
    if (error)
    {
        // report_yk_error();
    }

    if (st)
        free(st);

    if (yk && ! yk_close_key(yk))
    {
        // report_yk_error();
        exit_code = 2;
    }

    if (! yk_release())
    {
        // report_yk_error();
        exit_code = 2;
    }

    exit(exit_code);
}